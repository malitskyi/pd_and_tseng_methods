About
======

This is a supplementary code (in Python 3) for the paper Malitsky Y. "Chambolle-Pock and Tseng's methods: relationship and extension to the bilevel optimization" 

Usage
======
There are 2 problems: Elastic net minimization over an inconsistent linear system and projection of the given element onto the solution set of the nonnegative least squares problem.

Each problem is presented in the individual Jupyter Notebook file.  To
play with these problems you need Jypyter Notebook to be installed on
your computer.  Alternatively, you can read these notebooks using
http://nbviewer.jupyter.org/

File `algorithms.py` collects codes of all optimization algorithms.

File `opt_operators.py` includes codes for several proximal operators.

File `output.py` includes some configuration code of matplotlib for better outputs.

Folder `figures` collects all plots that were used in the paper.

