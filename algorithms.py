# algorithms.py

import numpy as np
import scipy as sp
import numpy.linalg as LA
from time import process_time
import matplotlib.pyplot as plt


def pd(J, prox_g, prox_f_conj, K,  x0, y0, sigma, tau, numb_iter=100):
    """
    Primal-dual algorithm of Pock and Chambolle for problem min_x
    max_y [<Kx,y> + g(x) - f*(y)]
    J denotes some function which we compute in every iteration to
    study perfomance. It may be energy, primal-dual gap, etc.

    """
    begin = process_time()
    theta = 1.0
    x, y, z = x0, y0, x0
    values = [J(x0, y0)]
    for i in range(numb_iter):
        x1 = prox_g(x - tau * K.T.dot(y), tau)
        z = x1 + theta * (x1 - x)
        y1 = prox_f_conj(y + sigma * K.dot(z), sigma)
        x = x1
        y = y1
        values.append(J(x, y))

    end = process_time()
    print("----- Primal-dual method -----")
    print("CPU time:", end - begin)
    return [values, x, y]


def halpern_method(df, prox_g, u, x0, la, numb_iter=100, display=False):
    """
    Solve problem: \min_x ||x-u||^2 where x \in \argmin g + f
    by the Halpern method.

    Return all iterates x_k into the array.

    """
    begin = process_time()
    iterates = [x0]
    x = x0
    for i in range(numb_iter):
        a = 1. / (i + 1)
        x = a * u + (1 - a) * prox_g(x - la * df(x), la)
        iterates.append(x)
    end = process_time()
    if display:
        print("----- Halpern method -----")
        print("CPU time:", end - begin)

    return np.array(iterates), x


def bi_new(prox_g, dh2, sigma, tau, x0, numb_iter=100, display=False):
    """
    Solve problem min_x g1(x)  s.t. x \in argmin g2(x)+h2(x).
    Here we assume that we know how to compute prox_g =
    prox_{a*g1+b*g2}

    sigma, tau: step sizes
    Return all iterates x_k, s_k into the array.


    """
    begin = process_time()
    x, s = x0, x0
    values = [[x, s]]
    for k in range(numb_iter):
        a = 2. / (k + 2)
        z = a * x + (1 - a) * s
        x = prox_g(x - (tau / a * dh2(z)), sigma, tau / a)
        s = a * x + (1 - a) * s
        values.append([x, s])

    end = process_time()
    if display:
        print("----- New method  -----")
        print("CPU time:", end - begin)

    return np.array(values), x, s


def fista(J, df, prox_g, x0, la, numb_iter=100):
    """
    Minimize function F(x) = f(x) + g(x) using FISTA
    """

    values = [J(x0)]
    x, y = x0, x0
    t = 1.

    for i in range(numb_iter):
        x1 = prox_g(y - la * df(y), la)
        t1 = 0.5 * (1 + np.sqrt(1 + 4 * t**2))
        y = x1 + (t - 1) / t1 * (x1 - x)
        x, t = x1, t1
        values.append(J(x))

    return values, x, y


def tseng_prox_grad(J, df, prox_g, x0, la, th, numb_iter=100):
    """
    Minimize function F(x) = f(x) + g(x) by Tseng's proximal gradient
    method with fixed stepsize la.  Takes J as some evaluation
    function for comparison.

    """

    values = [J(x0)]
    x, s = x0, x0

    for i in range(numb_iter):
        z = th * x + (1 - th) * s
        x = prox_g(x - la / th * df(z), la / th)
        s = th * x + (1 - th) * s
        values.append(J(x))

    return values, x, s
