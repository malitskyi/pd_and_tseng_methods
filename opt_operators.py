import numpy as np
import numpy.linalg as LA


def proj_ball(x, center, rad):
    """
    Compute projection of x onto the closed ball B(center, rad)
    """
    d = x - center
    dist = LA.norm(d)
    if dist <= rad:
        return x
    else:
        return rad * d / dist + center


def prox_norm_1(x, eps, u=0):
    """
    Find proximal operator of function eps*||x-u||_1
    """
    x1 = x + np.clip(u - x, -eps, eps)
    return x1


def prox_norm_2(x, eps, a=0):
    """
    Find proximal operator of function f = 0.5*eps*||x-a||**2,
    """
    return (x + eps * a) / (eps + 1)


def prox_norm_elastic_net(x, e1=1, e2=1):
    """
    Find proximal operator of the elastic net
    f = e1 * ||x||_1  + 0.5*e2*||x||**2
    """

    x1 = prox_norm_1(x / (1. + e2), e1 / (1. + e2))
    return x1


def project_nd(x, r=1):
    '''perform a pixel-wise projection onto r-radius balls. Here r = 1'''
    norm_x = np.sqrt(
        (x * x).sum(-1))  # Calculate L2 norm over the last array dimension
    nx = np.maximum(1.0, norm_x / r)
    return x / nx[..., np.newaxis]
